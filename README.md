# Пакет замены по тегам в строках/тексте

## Methods
- `Replace()` - нераспознанные теги остаются
- `ReplaceC()` - нераспознанные теги удаляются
- `ReplaceE()` - нераспознанные теги остаются, дополнительно возвращает массив ошибок
- `ReplaceCE()` - нераспознанные теги удаляются, дополнительно возвращает массив ошибок

## Usage:
```go
import "gitlab.com/leolab/go/replacer"

func hello() string {
    return "Hello"
}

func greet(name string) string {
    return "Hello "+name
}

func list(vals ...string) string{
    return "Some: "+strings.Join(vals,", ")
}

func main(){
    repl:=replacer.NewReplacer()
    //Predefined functions
    src:=`
        Date: %date%
        Year: %date.Y%
        Month: %date.M%
        Day: %date.D%
        Time: %time%
        Hour: %time.H%
        Minute: %time.M%
        Second: %time.S%
        Ms: %time.Z%
        Ns: %time.ns%

        %undefined%

        %hello%, %name%
        %greet Michael%
        %list one two three four%

    `

    //Extends functions
    repl.Add("name","John")
    repl.Add("hello",hello)
    repl.Add("greet",greet)
    repl.Add("list",list)

    {
        dst:=repl.Replace(src)
        fmt.Println(dst)
    }
    
    {
        dst:=repl.ReplaceC(src)
        fmt.Println(dst)
    }
    
    {
        dst,e:=repl.ReplaceE(src)
        if e!=nil{
            for _,er:=range e {
                fmt.Println("Error: ",er.Error())
            }
        }
        fmt.Println(dst)
    }

    {
        dst,e:=repl.ReplaceCE(src)
        if e!=nil{
            for _,er:=range e {
                fmt.Println("Error: ",er.Error())
            }
        }
        fmt.Println(dst)
    }

}
```