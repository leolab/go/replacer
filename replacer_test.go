package replacer

import (
	"fmt"
	"strings"
	"testing"
)

var src string = `
        Date: %date%
        Year: %date.Y%
        Month: %date.M%
        Day: %date.D%
        Time: %time%
        Hour: %time.H%
        Minute: %time.M%
        Second: %time.S%
        Ms: %time.Z%
        Ns: %time.ns%

        %undefined%

        %hello%, %name%
        %greet Michael%
        %list one two three four%

        %undeflist one two ...%
    `

var src1 string = `
	Date: $date$
	Day: $date.D$

	Line: $line$

	Code: $code$
`

func hello() string {
	return "Hello"
}

func greet(name string) string {
	return "Hello " + name
}

func list(vals ...string) string {
	return "Some: " + strings.Join(vals, ", ")
}

func TestNewReplacer(t *testing.T) {
	repl := NewReplacer()
	repl.Add("name", "John")
	repl.Add("hello", hello)
	repl.Add("greet", greet)
	repl.Add("list", list)

	{
		dst := repl.Replace(src)
		fmt.Println(dst)
	}

	{
		dst := repl.ReplaceC(src)
		fmt.Println(dst)
	}

	{
		dst, e := repl.ReplaceE(src)
		if e != nil {
			fmt.Println("Errors: ")
			for _, er := range e {
				fmt.Println("\t", er.Error())
			}
		}
		fmt.Println(dst)
	}

	{
		dst, e := repl.ReplaceCE(src)
		if e != nil {
			fmt.Println("Errors: ")
			for _, er := range e {
				fmt.Println("\t", er.Error())
			}
		}
		fmt.Println(dst)
	}

}

func TestReplace2(t *testing.T) {
	r := NewReplacer()
	e := r.Add("line", "123")
	if e != nil {
		fmt.Println("\t", e.Error())
	}
	r.Add("code", "NewCode")
	r.MChar = '$'

	dst := r.Replace(src1)
	fmt.Println(dst)
}
