package replacer

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
	"time"
)

type Replacer struct {
	MChar rune
	r     map[string]interface{}
}

func NewReplacer() *Replacer {
	r := &Replacer{
		MChar: '%',
		r:     make(map[string]interface{}),
	}
	r._init()
	return r
}

//Произвести замену, удалить неопознанные теги, вернуть ошибки
func (r *Replacer) ReplaceCE(s string) (string, []error) {
	ret, e := r._replace(s, true)
	if len(e) > 0 {
		return ret, e
	}
	return ret, nil
}

//Произвести замену, не менять неопознанные теги, вернуть ошибки
func (r *Replacer) ReplaceE(s string) (string, []error) {
	ret, e := r._replace(s, false)
	if len(e) > 0 {
		return ret, e
	}
	return ret, nil
}

//Произвести замену, удалить неопознанные теги
func (r *Replacer) ReplaceC(s string) string {
	ret, _ := r._replace(s, true)
	return ret
}

//Произвести замену, не менять неопознаные теги
func (r *Replacer) Replace(s string) string {
	ret, _ := r._replace(s, false)
	return ret
}

//Реальный метод замены
func (r *Replacer) _replace(s string, clr bool) (ret string, e []error) {
	e = make([]error, 0)
	b := bytes.NewBuffer([]byte(""))
	max := len(s)
	pi := 0
	for i := 0; i < max; {
		for i < max && s[i] != byte(r.MChar) {
			i++
		}
		b.WriteString(s[pi:i])
		pi = i
		i++
		if i > max {
			return b.String(), e
		}
		for i < max && s[i] != byte(r.MChar) {
			i++
		}
		key := strings.Split(s[pi+1:i], " ")

		//У ключа может быть параметр
		i++
		pi = i
		if v, ok := r.r[key[0]]; ok {
			switch vv := v.(type) {
			case string:
				b.WriteString(vv)
			case func() string: //Функция без параметра
				b.WriteString(vv())
			case func(string) string: //Функция с параметром
				if len(key) > 1 {
					b.WriteString(vv(key[1]))
				}
			case func(...string) string: //Функция с несколькими параметрами
				if len(key) > 1 {
					var args []reflect.Value
					for _, x := range key[1:] {
						args = append(args, reflect.ValueOf(x))
					}
					fn := reflect.ValueOf(vv)
					res := fn.Call(args)
					s := res[0].Interface().(string)
					b.WriteString(s)
				}
			default:
				//Unknown type
			}
		} else { //Если ключа не нашли -
			e = append(e, newErrorUndefinedTag(string(r.MChar)+strings.Join(key, " ")+string(r.MChar)))
			if clr { //Исключаем ключ
			} else { //не меняем ключ
				b.WriteString(string(r.MChar) + strings.Join(key, " ") + string(r.MChar))
			}
		}
	}
	return b.String(), e
}

func (r *Replacer) _init() {
	if r.r == nil {
		r.r = make(map[string]interface{})
	}
	r.r["date"] = func() string { return time.Now().Format("2006-01-02") }
	r.r["date.Y"] = func() string { return time.Now().Format("2006") }
	r.r["date.M"] = func() string { return time.Now().Format("01") }
	r.r["date.D"] = func() string { return time.Now().Format("02") }
	r.r["time"] = func() string { return time.Now().Format("15:04:05") }
	r.r["time.H"] = func() string { return time.Now().Format("15") }
	r.r["time.M"] = func() string { return time.Now().Format("04") }
	r.r["time.S"] = func() string { return time.Now().Format("05") }
	r.r["time.Z"] = func() string { return time.Now().Format("000") }
	r.r["time.ns"] = func() string { return time.Now().Format("000000") }
}

func (r *Replacer) Add(key string, fn interface{}) error {
	switch v := fn.(type) {
	case string:
		r.r[key] = v
	case func() string:
		r.r[key] = v
	case func(string) string:
		r.r[key] = v
	case func(...string) string:
		r.r[key] = v
	default:
		return newErrorUnsupportedReplaceType(fmt.Sprintf("%T", v))
	}
	return nil
}

func (r *Replacer) Del(key string) error {
	if _, ok := r.r[key]; ok {
		delete(r.r, key)
	} else {
		return newErrorKeyNotFound(key)
	}
	return nil
}
