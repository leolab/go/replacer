package replacer

//ErrorUndefinedTag
type ErrorUndefinedTag struct{ Msg string }

func (e ErrorUndefinedTag) Error() string { return e.Msg }

func newErrorUndefinedTag(msg string) ErrorUndefinedTag {
	return ErrorUndefinedTag{Msg: msg}
}

//ErrorUnsupportedReplaceType
type ErrorUnsupportedReplaceType struct{ Msg string }

func (e ErrorUnsupportedReplaceType) Error() string { return e.Msg }

func newErrorUnsupportedReplaceType(msg string) ErrorUnsupportedReplaceType {
	return ErrorUnsupportedReplaceType{Msg: msg}
}

//ErrorKeyNotFound
type ErrorKeyNotFound struct{ Msg string }

func (e ErrorKeyNotFound) Error() string { return e.Msg }

func newErrorKeyNotFound(msg string) ErrorKeyNotFound { return ErrorKeyNotFound{Msg: msg} }
